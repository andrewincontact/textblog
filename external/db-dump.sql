-- SQL Manager Lite for PostgreSQL 5.9.5.52424
-- ---------------------------------------
-- Host      : localhost
-- Database  : blog
-- Version   : PostgreSQL 11.5, compiled by Visual C++ build 1914, 64-bit



SET check_function_bodies = false;
--
-- Structure for table category (OID = 24599) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public.category (
    id bigserial NOT NULL,
    name varchar(20) NOT NULL,
    url varchar(20) NOT NULL,
    articles integer DEFAULT 0 NOT NULL
)
WITH (oids = false);
--
-- Structure for table article (OID = 24607) : 
--
CREATE TABLE public.article (
    id bigserial NOT NULL,
    title varchar(255) NOT NULL,
    url varchar(255) NOT NULL,
    logo varchar(255) NOT NULL,
    "desc" varchar(255) NOT NULL,
    content text NOT NULL,
    id_category integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    views bigint DEFAULT 0 NOT NULL,
    comments integer DEFAULT 0 NOT NULL
)
WITH (oids = false);
--
-- Structure for table account (OID = 24628) : 
--
CREATE TABLE public.account (
    id bigserial NOT NULL,
    email varchar(100) NOT NULL,
    name varchar(30) NOT NULL,
    avatar varchar(255),
    created timestamp without time zone DEFAULT now() NOT NULL
)
WITH (oids = false);
--
-- Structure for table comment (OID = 24637) : 
--
CREATE TABLE public.comment (
    id bigserial NOT NULL,
    id_account bigint NOT NULL,
    id_article bigint NOT NULL,
    content text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
)
WITH (oids = false);
--
-- Definition for index category_url_uindex (OID = 24604) : 
--
CREATE UNIQUE INDEX category_url_uindex ON public.category USING btree (url);
--
-- Definition for index comment_idx (OID = 24662) : 
--
CREATE INDEX comment_idx ON public.comment USING btree (id_article);
--
-- Definition for index article_idx (OID = 24663) : 
--
CREATE INDEX article_idx ON public.article USING btree (id_category);
--
-- Definition for index comment_idx1 (OID = 24664) : 
--
CREATE INDEX comment_idx1 ON public.comment USING btree (id_account);
--
-- Definition for index article_pk (OID = 24617) : 
--
ALTER TABLE ONLY article
    ADD CONSTRAINT article_pk
    PRIMARY KEY (id);
--
-- Definition for index category_pk (OID = 24624) : 
--
ALTER TABLE ONLY category
    ADD CONSTRAINT category_pk
    PRIMARY KEY (id);
--
-- Definition for index account_pk (OID = 24633) : 
--
ALTER TABLE ONLY account
    ADD CONSTRAINT account_pk
    PRIMARY KEY (id);
--
-- Definition for index comment_pk (OID = 24645) : 
--
ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_pk
    PRIMARY KEY (id);
--
-- Definition for index article_fk (OID = 24647) : 
--
ALTER TABLE ONLY article
    ADD CONSTRAINT article_fk
    FOREIGN KEY (id_category) REFERENCES category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index comment_fk (OID = 24652) : 
--
ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_fk
    FOREIGN KEY (id_account) REFERENCES account(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index comment_fk1 (OID = 24657) : 
--
ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_fk1
    FOREIGN KEY (id_article) REFERENCES article(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Data for sequence public.category_id_seq (OID = 24597)
--
SELECT pg_catalog.setval('category_id_seq', 1, false);
--
-- Data for sequence public.article_id_seq (OID = 24605)
--
SELECT pg_catalog.setval('article_id_seq', 1, false);
--
-- Data for sequence public.account_id_seq (OID = 24626)
--
SELECT pg_catalog.setval('account_id_seq', 1, false);
--
-- Data for sequence public.comment_id_seq (OID = 24635)
--
SELECT pg_catalog.setval('comment_id_seq', 1, false);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
